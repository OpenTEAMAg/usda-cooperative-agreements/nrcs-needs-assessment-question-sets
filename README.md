# NRCS Needs Assessment Question Sets


## Structure of this repository
- This repository 

## Contribution


## Authorship:
- Each question set is in a directory also containing a list of authorships. When you contribute changes to the questionset, add your name to that question set's authorship document.




## Acknowledgements:
This work is funded in part by Cooperative Agreement between NRCS and OpenTEAM, Award #: NR223A750008C003
